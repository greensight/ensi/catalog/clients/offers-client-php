# # OfferFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_publish** | **bool** | Признак активности для витрины | [optional] 
**price** | **int** | Цена предложения в коп. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


