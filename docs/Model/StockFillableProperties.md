# # StockFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qty** | **float** | Количество товара для резервирования | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


