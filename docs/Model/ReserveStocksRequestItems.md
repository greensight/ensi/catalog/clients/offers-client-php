# # ReserveStocksRequestItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store_id** | **int** | ID склада | 
**offer_id** | **int** | ID товарного предложения | 
**qty** | **float** | Количество товара для резервирования | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


