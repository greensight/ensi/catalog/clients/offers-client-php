# # SearchFailedJobsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\OffersClient\Dto\FailedJob[]**](FailedJob.md) |  | 
**meta** | [**\Ensi\OffersClient\Dto\SearchOffersResponseMeta**](SearchOffersResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


