# # OfferReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор оффера | 
**product_id** | **int** | Идентификатор товара, для которого создается оффер | 
**is_active** | **bool** | Активность оффера (Вычисляется по активности товара, наличию стоков и т.п.) | 
**is_real_active** | **bool** | Итоговая активность оффера, по которой оффер попадет на витрину | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания оффера | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления оффера | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


