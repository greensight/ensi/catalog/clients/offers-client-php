# # StockReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор стока | 
**store_id** | **int** | ID склада | 
**offer_id** | **int** | ID товарного предложения | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания остатка | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления остатка | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


