# # OfferIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stocks** | [**\Ensi\OffersClient\Dto\Stock[]**](Stock.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


