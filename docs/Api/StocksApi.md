# Ensi\OffersClient\StocksApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStock**](StocksApi.md#getStock) | **GET** /stocks/stocks/{id} | Запрос остатка по ID
[**patchStock**](StocksApi.md#patchStock) | **PATCH** /stocks/stocks/{id} | Запрос на обновление отдельных полей остатка
[**reserveStocks**](StocksApi.md#reserveStocks) | **POST** /stocks/stocks:reserve | Резервирование стоков
[**searchStocks**](StocksApi.md#searchStocks) | **POST** /stocks/stocks:search | Поиск остатков, удовлетворяющих фильтру



## getStock

> \Ensi\OffersClient\Dto\StockResponse getStock($id, $include)

Запрос остатка по ID

Запрос остатка по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStock($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->getStock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OffersClient\Dto\StockResponse**](../Model/StockResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStock

> \Ensi\OffersClient\Dto\StockResponse patchStock($id, $patch_stock_request)

Запрос на обновление отдельных полей остатка

Запрос на обновление отдельных полей остатка

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_stock_request = new \Ensi\OffersClient\Dto\PatchStockRequest(); // \Ensi\OffersClient\Dto\PatchStockRequest | 

try {
    $result = $apiInstance->patchStock($id, $patch_stock_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->patchStock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_stock_request** | [**\Ensi\OffersClient\Dto\PatchStockRequest**](../Model/PatchStockRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\StockResponse**](../Model/StockResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## reserveStocks

> \Ensi\OffersClient\Dto\EmptyDataResponse reserveStocks($reserve_stocks_request)

Резервирование стоков

Резервирование стоков

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reserve_stocks_request = new \Ensi\OffersClient\Dto\ReserveStocksRequest(); // \Ensi\OffersClient\Dto\ReserveStocksRequest | 

try {
    $result = $apiInstance->reserveStocks($reserve_stocks_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->reserveStocks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reserve_stocks_request** | [**\Ensi\OffersClient\Dto\ReserveStocksRequest**](../Model/ReserveStocksRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStocks

> \Ensi\OffersClient\Dto\SearchStocksResponse searchStocks($search_stocks_request)

Поиск остатков, удовлетворяющих фильтру

Поиск остатков, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_stocks_request = new \Ensi\OffersClient\Dto\SearchStocksRequest(); // \Ensi\OffersClient\Dto\SearchStocksRequest | 

try {
    $result = $apiInstance->searchStocks($search_stocks_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->searchStocks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_stocks_request** | [**\Ensi\OffersClient\Dto\SearchStocksRequest**](../Model/SearchStocksRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\SearchStocksResponse**](../Model/SearchStocksResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

