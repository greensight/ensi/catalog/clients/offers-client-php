# Ensi\OffersClient\OffersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOffer**](OffersApi.md#getOffer) | **GET** /offers/offers/{id} | Запрос оффера по ID
[**migrateOffers**](OffersApi.md#migrateOffers) | **POST** /offers/offers:migrate | Запуск синхронизации состояния офферов относительно PIM и складов
[**patchOffer**](OffersApi.md#patchOffer) | **PATCH** /offers/offers/{id} | Запрос на обновление отдельных полей оффера
[**searchOffers**](OffersApi.md#searchOffers) | **POST** /offers/offers:search | Поиск офферов, удовлетворяющих фильтру



## getOffer

> \Ensi\OffersClient\Dto\OfferResponse getOffer($id, $include)

Запрос оффера по ID

Запрос оффера по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getOffer($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->getOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OffersClient\Dto\OfferResponse**](../Model/OfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## migrateOffers

> \Ensi\OffersClient\Dto\EmptyDataResponse migrateOffers()

Запуск синхронизации состояния офферов относительно PIM и складов

Запуск синхронизации состояния офферов относительно PIM и складов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->migrateOffers();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->migrateOffers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchOffer

> \Ensi\OffersClient\Dto\OfferResponse patchOffer($id, $patch_offer_request)

Запрос на обновление отдельных полей оффера

Запрос на обновление отдельных полей оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_offer_request = new \Ensi\OffersClient\Dto\PatchOfferRequest(); // \Ensi\OffersClient\Dto\PatchOfferRequest | 

try {
    $result = $apiInstance->patchOffer($id, $patch_offer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->patchOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_offer_request** | [**\Ensi\OffersClient\Dto\PatchOfferRequest**](../Model/PatchOfferRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\OfferResponse**](../Model/OfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOffers

> \Ensi\OffersClient\Dto\SearchOffersResponse searchOffers($search_offers_request)

Поиск офферов, удовлетворяющих фильтру

Поиск офферов, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_offers_request = new \Ensi\OffersClient\Dto\SearchOffersRequest(); // \Ensi\OffersClient\Dto\SearchOffersRequest | 

try {
    $result = $apiInstance->searchOffers($search_offers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->searchOffers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_offers_request** | [**\Ensi\OffersClient\Dto\SearchOffersRequest**](../Model/SearchOffersRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\SearchOffersResponse**](../Model/SearchOffersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

