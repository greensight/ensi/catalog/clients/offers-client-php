# Ensi\OffersClient\CommonApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchFailedJobs**](CommonApi.md#searchFailedJobs) | **POST** /common/failed-jobs:search | Search for objects of FailedJob
[**syncEntities**](CommonApi.md#syncEntities) | **POST** /common/entities:sync | Запуск обновления данных сущностей из мастер систем



## searchFailedJobs

> \Ensi\OffersClient\Dto\SearchFailedJobsResponse searchFailedJobs($search_failed_jobs_request)

Search for objects of FailedJob

Search for objects of FailedJob

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_failed_jobs_request = new \Ensi\OffersClient\Dto\SearchFailedJobsRequest(); // \Ensi\OffersClient\Dto\SearchFailedJobsRequest | 

try {
    $result = $apiInstance->searchFailedJobs($search_failed_jobs_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->searchFailedJobs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_failed_jobs_request** | [**\Ensi\OffersClient\Dto\SearchFailedJobsRequest**](../Model/SearchFailedJobsRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\SearchFailedJobsResponse**](../Model/SearchFailedJobsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## syncEntities

> \Ensi\OffersClient\Dto\EmptyDataResponse syncEntities()

Запуск обновления данных сущностей из мастер систем

Запуск обновления данных сущностей из мастер систем

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->syncEntities();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->syncEntities: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

