<?php

namespace Ensi\OffersClient;

class OffersClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\OffersClient\Api\CommonApi',
        '\Ensi\OffersClient\Api\OffersApi',
        '\Ensi\OffersClient\Api\StocksApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\OffersClient\Dto\ErrorResponse',
        '\Ensi\OffersClient\Dto\MassOperationResultDataErrors',
        '\Ensi\OffersClient\Dto\RequestBodyMassDelete',
        '\Ensi\OffersClient\Dto\PatchOfferRequest',
        '\Ensi\OffersClient\Dto\OfferReadonlyProperties',
        '\Ensi\OffersClient\Dto\StockResponse',
        '\Ensi\OffersClient\Dto\Stock',
        '\Ensi\OffersClient\Dto\MassOperationResultData',
        '\Ensi\OffersClient\Dto\EmptyDataResponse',
        '\Ensi\OffersClient\Dto\Offer',
        '\Ensi\OffersClient\Dto\FailedJobProperties',
        '\Ensi\OffersClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\OffersClient\Dto\SearchFailedJobsRequest',
        '\Ensi\OffersClient\Dto\MassOperationResult',
        '\Ensi\OffersClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\OffersClient\Dto\OfferIncludes',
        '\Ensi\OffersClient\Dto\ErrorResponse2',
        '\Ensi\OffersClient\Dto\StockReadonlyProperties',
        '\Ensi\OffersClient\Dto\SearchOffersRequest',
        '\Ensi\OffersClient\Dto\SearchStocksRequest',
        '\Ensi\OffersClient\Dto\StockFillableProperties',
        '\Ensi\OffersClient\Dto\RequestBodyPagination',
        '\Ensi\OffersClient\Dto\PatchStockRequest',
        '\Ensi\OffersClient\Dto\SearchOffersResponseMeta',
        '\Ensi\OffersClient\Dto\ReserveStocksRequest',
        '\Ensi\OffersClient\Dto\OfferFillableProperties',
        '\Ensi\OffersClient\Dto\OfferResponse',
        '\Ensi\OffersClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\OffersClient\Dto\RequestBodyCursorPagination',
        '\Ensi\OffersClient\Dto\FailedJob',
        '\Ensi\OffersClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\OffersClient\Dto\Error',
        '\Ensi\OffersClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\OffersClient\Dto\SearchFailedJobsResponse',
        '\Ensi\OffersClient\Dto\SearchOffersResponse',
        '\Ensi\OffersClient\Dto\PaginationTypeEnum',
        '\Ensi\OffersClient\Dto\ResponseBodyPagination',
        '\Ensi\OffersClient\Dto\SearchStocksResponse',
        '\Ensi\OffersClient\Dto\ReserveStocksRequestItems',
        '\Ensi\OffersClient\Dto\ModelInterface',
    ];

    /** @var string */
    public static $configuration = '\Ensi\OffersClient\Configuration';
}
